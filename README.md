## The node.js example app

[![Quality gate](https://sonarcloud.io/api/project_badges/quality_gate?project=sathish-mr_the-example-app.nodejs)](https://sonarcloud.io/dashboard?id=sathish-mr_the-example-app.nodejs)

[![Bugs](https://sonarcloud.io/api/project_badges/measure?project=sathish-mr_the-example-app.nodejs&metric=bugs)](https://sonarcloud.io/dashboard?id=sathish-mr_the-example-app.nodejs)

[![Code Smells](https://sonarcloud.io/api/project_badges/measure?project=sathish-mr_the-example-app.nodejs&metric=code_smells)](https://sonarcloud.io/dashboard?id=sathish-mr_the-example-app.nodejs)

[![Coverage](https://sonarcloud.io/api/project_badges/measure?project=sathish-mr_the-example-app.nodejs&metric=coverage)](https://sonarcloud.io/dashboard?id=sathish-mr_the-example-app.nodejs)

[![Duplicated Lines (%)](https://sonarcloud.io/api/project_badges/measure?project=sathish-mr_the-example-app.nodejs&metric=duplicated_lines_density)](https://sonarcloud.io/dashboard?id=sathish-mr_the-example-app.nodejs)

[![Lines of Code](https://sonarcloud.io/api/project_badges/measure?project=sathish-mr_the-example-app.nodejs&metric=ncloc)](https://sonarcloud.io/dashboard?id=sathish-mr_the-example-app.nodejs)

[![Maintainability Rating](https://sonarcloud.io/api/project_badges/measure?project=sathish-mr_the-example-app.nodejs&metric=sqale_rating)](https://sonarcloud.io/dashboard?id=sathish-mr_the-example-app.nodejs)

[![Technical Debt](https://sonarcloud.io/api/project_badges/measure?project=sathish-mr_the-example-app.nodejs&metric=sqale_index)](https://sonarcloud.io/dashboard?id=sathish-mr_the-example-app.nodejs)

[![Vulnerabilities](https://sonarcloud.io/api/project_badges/measure?project=sathish-mr_the-example-app.nodejs&metric=vulnerabilities)](https://sonarcloud.io/dashboard?id=sathish-mr_the-example-app.nodejs)
